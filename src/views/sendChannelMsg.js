// DISCORD BOT
const disClient = require('../bot/bot')


function sendChannelMsg(req, res) {
    let channelId = req.params.channelId
    let msgText = req.body.msg

    let channel = disClient.channels.cache.get(channelId)
    channel.send(msgText)

    res.sendStatus(201)
}

module.exports = sendChannelMsg
