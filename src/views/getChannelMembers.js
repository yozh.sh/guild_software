// DISCORD BOT
const disClient = require('../bot/bot')


function getChannelMembers(req, res) {
    let guildID = req.params.guildId
    let guild = disClient.guilds.cache.get(guildID)
    guild.members.fetch({force: true}).then(function(result){
        let data = []
        result.filter(user => user.user.bot === false).forEach(user => {
            user.user.avatarURL()
            data.push({...user.user, avatar: user.user.avatarURL()})
        })
        res.json(data)
    })
    
}

module.exports = getChannelMembers
