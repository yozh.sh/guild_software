const { use } = require('../../app')
const client = require('../bot/bot')
const disClient = require('../bot/bot')


function sendPrivateMsg(req, res) {
    let userId = req.params.userId
    let msgText = req.body.msg

    disClient.users.fetch(userId)
        .then(user => {
            user.send(msgText)
        }, reason => {
            console.log("ERROR")
        }
    )

    res.sendStatus(202)
}

module.exports = sendPrivateMsg
