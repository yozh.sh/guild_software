const MessageSchema = {
    type: 'object',
    required: ['msg'],
    properties: {
        msg: {
            type: 'string'
        }
    }
}

module.exports = MessageSchema
