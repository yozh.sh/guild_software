const express = require('express')
const app = express()
const conf = require('./config.json')
const bodyParser = require('body-parser')
const { Validator, ValidationError } = require('express-json-validator-middleware');


// VIEWS
const sendChannelMsg = require('./src/views/sendChannelMsg')
const sendPrivateMsg = require('./src/views/sendPrivateMsg')
const getChannelMembers = require('./src/views/getChannelMembers')

// SCHEMAS
const msgSchema = require('./src/schema/messages')

// Initialize a Validator instance first
const validator = new Validator({allErrors: true}); // pass in options to the Ajv instance

// Define a shortcut function
const validate = validator.validate;

app.use(bodyParser.json())


// Send messages

// Send to channel

app.post("/sendChannelMsg/:channelId", validate({body: msgSchema}), sendChannelMsg)

// Send to user
app.post("/sendUserPrivateMsg/:userId", validate({body: msgSchema}), sendPrivateMsg)

app.get('/getMembers/:guildId', getChannelMembers)

app.listen(conf.PORT, () => {
    console.log("App is UP!")
})

module.exports = app
